﻿using Microsoft.AspNetCore.Mvc;

namespace mam.web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
