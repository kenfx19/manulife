﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using mam.web.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace mam.web.Controllers
{
    [Route("[controller]")]
    public class ApiController : Controller
    {
        private readonly ILogger<ApiController> _logger;
        private Setting _settings;

        public ApiController(ILoggerFactory loggerFactory, IOptionsSnapshot<Setting> settings)
        {
            _logger = loggerFactory.CreateLogger<ApiController>();
            _settings = settings.Value;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("stats")]
        public IActionResult Get([FromQuery]DateTime? date)
        {
            var stats = new List<WebsiteStat>();
            var exclusions = new List<Exclusion>();
            var dataFile = _settings.DataStoreFilePath;
            var exclusionFile = _settings.ExclusionFilePath;

            _logger.LogInformation(dataFile);
            _logger.LogInformation(exclusionFile);

            if (System.IO.File.Exists(dataFile))
            {
                using (var reader = new StreamReader(dataFile))
                using (var csv = new CsvHelper.CsvReader(reader))
                {
                    csv.Configuration.Delimiter = "|";
                    csv.Configuration.RegisterClassMap<WebsiteStatMapper>();
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.MissingFieldFound = null;
                    stats = csv.GetRecords<WebsiteStat>().ToList();

                }
            }

            if (System.IO.File.Exists(_settings.ExclusionFilePath))
            {
                using (var reader = new StreamReader(exclusionFile))
                using (var csv = new CsvHelper.CsvReader(reader))
                {
                    csv.Configuration.Delimiter = "|";
                    csv.Configuration.RegisterClassMap<ExclusionMapper>();
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.MissingFieldFound = null;
                    exclusions = csv.GetRecords<Exclusion>().ToList();
                }
            }

            var excludedHosts = exclusions
                                    .Where(e => !(Convert.ToDateTime(DateTime.TryParse(e.ExcludedTill, out DateTime value) ? e.ExcludedTill : DateTime.Now.ToString()).Date < DateTime.Now.Date))
                                    .Select(e => e.Host)
                                    .ToList();

            if (date.HasValue)
            {
                stats = stats
                    .Where(s => date.Value.ToString("yyyy-MM-dd") == s.Date)
                    .Where(s => !excludedHosts.Any(e => s.Website.Contains(e)))
                    .OrderByDescending(x => x.Visits).ToList();
            }
            else
            {
                stats = stats
                    .Where(s => !excludedHosts.Any(e => s.Website.Contains(e)))
                    .OrderByDescending(x => x.Visits).ToList();
            }

            return new JsonResult(stats);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("exclusions")]
        public async Task<IActionResult> UpdateExclusions()
        {
            var exclusionApiUrl = _settings.ExclusionApiUri;

            _logger.LogInformation(exclusionApiUrl);

            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetAsync(exclusionApiUrl);
                    response.EnsureSuccessStatusCode();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var exclusions = JsonConvert.DeserializeObject<List<Exclusion>>(stringResult);
                    using (var writer = new StreamWriter(_settings.ExclusionFilePath))
                    using (var csv = new CsvHelper.CsvWriter(writer))
                    {
                        csv.Configuration.Delimiter = "|";
                        csv.Configuration.RegisterClassMap<ExclusionMapper>();
                        csv.Configuration.HasHeaderRecord = true;
                        csv.WriteRecords<Exclusion>(exclusions);
                    }
                    return Ok(exclusions);
                }
                catch (HttpRequestException httpRequestException)
                {
                    return BadRequest($"Failed to call exclusion api {exclusionApiUrl}: {httpRequestException.Message}");
                }
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("stats")]
        public async Task<IActionResult> UploadStats(IFormFile file)
        {
            var filePath = _settings.UploadFilePath;

            _logger.LogInformation(filePath);

            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            return Ok();
        }
    }
}
