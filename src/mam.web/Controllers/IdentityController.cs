﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using mam.web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace mam.web.Controllers
{
    [Route("[controller]")]
    public class IdentityController : Controller
    {
        private readonly ILogger<IdentityController> _logger;
        private Setting _settings;

        public IdentityController(ILoggerFactory loggerFactory, IOptionsSnapshot<Setting> settings)
        {
            _logger = loggerFactory.CreateLogger<IdentityController>();
            _settings = settings.Value;
        }

        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Token([FromBody]Dictionary<string, string> user)
        {
            var username = user.GetValueOrDefault("username");
            if (await ValidateUserAsync(username))
            {
                return new ObjectResult(GenerateToken(username));
            }
            return BadRequest();
        }

        private async Task<bool> ValidateUserAsync(string username)
        {
            var users = new List<string>();
            var userFile = _settings.UserFilePath;

            _logger.LogInformation(userFile);

            if (System.IO.File.Exists(userFile))
            {
                using (var stream = new StreamReader(userFile))
                {
                    var userStore = await stream.ReadToEndAsync();
                    users = userStore.Split(',').ToList();
                }
            }

            return users.Any(user => user == username);
        }

        private string GenerateToken(string username)
        {
            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString()),
            };

            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("myappsecretforsha256"));
            SigningCredentials signingCredential = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            JwtHeader jwtHeader = new JwtHeader(signingCredential);
            JwtPayload jwtPayload = new JwtPayload(claims);
            JwtSecurityToken token = new JwtSecurityToken(jwtHeader, jwtPayload);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
