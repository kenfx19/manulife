﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using mam.web.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using static System.Net.WebRequestMethods;

namespace mam.web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Thread watcher = new Thread(StatImportWatcher);
            watcher.Start();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        private static void StatImportWatcher()
        {
            while (true)
            {
                try
                {
                    var stats = new List<WebsiteStat>();
                    var uploadedFile = Environment.GetEnvironmentVariable("UploadFilePath");
                    var dataFile = Environment.GetEnvironmentVariable("DataStoreFilePath");

                    if (System.IO.File.Exists(uploadedFile))
                    {
                        using (var reader = new StreamReader(uploadedFile))
                        using (var csv = new CsvHelper.CsvReader(reader))
                        {
                            csv.Configuration.Delimiter = "|";
                            csv.Configuration.RegisterClassMap<WebsiteStatMapper>();
                            csv.Configuration.HasHeaderRecord = true;
                            csv.Configuration.MissingFieldFound = null;
                            stats = csv.GetRecords<WebsiteStat>().ToList();

                        }
                    }
                    if (stats.Count > 0)
                    {
                        if (System.IO.File.Exists(dataFile))
                        {
                            using (var writer = new StreamWriter(dataFile, true))
                            using (var csv = new CsvHelper.CsvWriter(writer))
                            {
                                csv.Configuration.Delimiter = "|";
                                csv.Configuration.RegisterClassMap<WebsiteStatMapper>();
                                csv.Configuration.HasHeaderRecord = false;
                                csv.WriteRecords<WebsiteStat>(stats);
                            }
                        }
                        else
                        {
                            using (var writer = new StreamWriter(dataFile))
                            using (var csv = new CsvHelper.CsvWriter(writer))
                            {
                                csv.Configuration.Delimiter = "|";
                                csv.Configuration.RegisterClassMap<WebsiteStatMapper>();
                                csv.Configuration.HasHeaderRecord = true;
                                csv.WriteRecords<WebsiteStat>(stats);
                            }
                        }
                    }

                    System.IO.File.Delete(uploadedFile);
                }
                catch (System.Exception)
                {
                }
                finally
                {
                    Thread.Sleep(10000);
                }
            }
        }
    }
}
