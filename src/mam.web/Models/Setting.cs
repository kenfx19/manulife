using System;

namespace mam.web.Models
{
    [Serializable]
    public class Setting
    {
        public string DataStoreFilePath { get; set; }
        public string ExclusionFilePath { get; set; }
        public string UserFilePath { get; set; }      
        public string UploadFilePath { get; set; }
        public string ExclusionApiUri { get; set; }
    }

}