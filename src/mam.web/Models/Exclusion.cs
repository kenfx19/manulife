using System;

namespace mam.web.Models
{
    [Serializable]
    public class Exclusion
    {
        public string Host { get; set; }
        public string ExcludedSince { get; set; }
        public string ExcludedTill { get; set; }
    }

     public class ExclusionMapper : CsvHelper.Configuration.ClassMap<Exclusion>
    {
        public ExclusionMapper()
        {
            Map(x => x.Host).Name("host").Index(0);
            Map(x => x.ExcludedSince).Name("excludedSince").Index(1);
            Map(x => x.ExcludedTill).Name("excludedTill").Index(2);
        }
    }
}