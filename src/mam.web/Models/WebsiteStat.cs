using System;

namespace mam.web.Models
{
    [Serializable]
    public class WebsiteStat
    {
        public string Date { get; set; }
        public string Website { get; set; }
        public long Visits { get; set; }
    }

     public class WebsiteStatMapper : CsvHelper.Configuration.ClassMap<WebsiteStat>
    {
        public WebsiteStatMapper()
        {
            Map(x => x.Date).Name("date").Index(0);
            Map(x => x.Website).Name("website").Index(1);
            Map(x => x.Visits).Name("visits").Index(2);
        }
    }
}